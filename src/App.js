import React, { useState } from 'react';

import './App.css';

const App = () => {

    const [ username, setUsername ] = useState('')
    const [ password, setPassword ] = useState('')
    const [ showPassword, setShowPassword ] = useState(true)

    console.log(showPassword);
    return (
        <div>
            <input
                type="text"
                placeholder="Enter username"
                value={username}
                onChange={ e => setUsername(e.target.value) }
            />

            <input
                type={showPassword ? 'password' : 'text'}
                placeholder="Enter password"
                value={password}
                onChange={ e => setPassword(e.target.value) }
            />
            <h2>{username}</h2>
            {/* If he showPassword is true then we display SHOW else we display Not SHOW */}
            { 
            // showPassword ? ( <h2>{password.split('').map((letter) => '*')}</h2>
                showPassword ? ( <h2>{'*'.repeat(password.length)}</h2>
            ) : (
                <h2>{password}</h2>
            )}
            
            <button onClick={(e) => {
                setShowPassword(!showPassword)  
            }}>Show/Hide Password</button>
        </div>
    )
}

export default App;